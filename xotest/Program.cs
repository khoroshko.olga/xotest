using System;
using System.Reflection;

namespace xotest
{
    internal class Program
    {
        const int a = 3;
        static string[,] gamePoleMap = new string[a, a];
        //Отрисовка игррового поля
        static void Display()
        {
            Console.Clear();
            for (int i = 0; i < a; i++)
            {
                for (int j = 0; j < a; j++)
                {
                    Console.Write("|" + gamePoleMap[i, j] + "|");
                }
                Console.WriteLine();
                Console.WriteLine("---------");
            }
        }

        static void Default()
        {
            for (int i = 0; i < a; i++)
            {
                for (int j = 0; j < a; j++)
                {
                    gamePoleMap[i, j] = "&";
                }
            }
        }

        //Ход компа
        static bool PCStep(string pcFigure)
        {
            Random rand = new Random();
            int indexI = rand.Next(0, a);
            int indexJ = rand.Next(0, a);
            bool res = Check(indexI, indexJ);
            if (!res)
            {
                PCStep(pcFigure);
            }
            else
                gamePoleMap[indexI, indexJ] = pcFigure;

            var winner = GetWinner(pcFigure);
            if (winner)
                Console.WriteLine("Победитель комп");
            return winner;
        }

        static bool Check(int indexI, int indexJ)
        {
            if (gamePoleMap[indexI, indexJ] != "X" && gamePoleMap[indexI, indexJ] != "0")
            {
                return true;
            }
            return false;
        }

        static bool GetWinner(string figure)
        {
            int hor;
            int vert;
            int diag = 0;
            int diag2 = 0;
            for (int i = 0; i < a; i++)
            {
                hor = 0;
                vert = 0;
                for (int j = 0; j < a; j++)
                {
                    if (gamePoleMap[i, j] == figure)
                    {
                        hor++;
                        if (hor == a)
                        {
                            return true;
                        }
                    }

                    if (gamePoleMap[j, i] == figure)
                    {
                        vert++;
                        if (vert == a)
                        {
                            return true;
                        }
                    }
                }

                if (gamePoleMap[i, i] == figure)
                {
                    diag++;
                    if (diag == a)
                    {
                        return true;
                    }
                }

                if (gamePoleMap[i, a - 1 - i] == figure)
                {
                    diag2++;
                    if (diag2 == a)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        static bool ExistEmpty()
        {
            var valueExists = Array.Exists(gamePoleMap.Cast<string>().ToArray(), element => element == "&");
            if (!valueExists)
                Console.WriteLine("Ничья");
            return valueExists;
        }

        static bool GetUserStep(string figure)
        {
            int indexI;
            int indexJ;
            while (true)
            {
                indexI = -1;
                indexJ = -1;
                Console.WriteLine("Сделайте ход. Выберете два значения через ,");
                var input = Console.ReadLine();
                if (input != null)
                {
                    var mas = input.Split(',');
                    if (mas.Length == 2)
                    {
                        if (mas[0] != null)
                        {
                            var resIndexI = Int32.TryParse(mas[0], out indexI);
                        }
                        if (mas[1] != null)
                        {
                            var resIndexJ = Int32.TryParse(mas[1], out indexJ);
                        }
                    }
                }

                if (indexI != -1 && indexJ != -1)
                {
                    var resCheck = Check(indexI, indexJ);
                    if (resCheck)
                    {
                        gamePoleMap[indexI, indexJ] = figure;
                        break;
                    }
                }
            }

            var res = GetWinner(figure);
            if (res)
                Console.WriteLine("Победитель юзер");
            return res;
        }

        static (string, string) GetFigure()
        {
            string pcFigure = "";
            string userFigure = "";
            while (true)
            {
                Console.WriteLine("Сделайте выбор фигуры. Х или 0");
                userFigure = Console.ReadLine();
                if (userFigure == "X" || userFigure == "0")
                {
                    pcFigure = userFigure == "X" ? "0" : "X";
                    break;
                }
            }
            return (userFigure, pcFigure);
        }

        static void Main(string[] args)
        {
            Default();
            Display();
            (string, string) figure = GetFigure();
            do
            {
                var res = true;
                res = ExistEmpty();
                if (!res)
                    break;

                res = GetUserStep(figure.Item1);
                Display();
                if (res)
                    break;

                res = ExistEmpty();
                if (!res)
                    break;

                res = PCStep(figure.Item2);
                Display();
                if (res)
                    break;
            }
            while (true);
        }
    }
}